#If the labels are present, then this gives the bounding boxes and confusion matrix as a csv
python inference_image_dir.py --num_classes 2 --device cpu --model /home/user/sandbox/turtles/detr_base/turtle-detection/outputs/checkpoint.pth --conf_thresh 0.85 --img_dir /home/user/sandbox/turtles/detr_base/turtle-detection/data/test/only_images/ --new_ann_dir /home/user/sandbox/turtles/detr_base/turtle-detection/results/new_annotations/
#python inference_image_dir.py --num_classes 2 --device cpu --model /home/user/sandbox/turtles/detr_base/turtle-detection/outputs/checkpoint.pth --conf_thresh 0.85 --img_dir /home/user/sandbox/turtles/detr_base/turtle-detection/data/test/ --new_ann_dir /home/user/sandbox/turtles/detr_base/turtle-detection/results/new_annotations/

