#If the labels are present, then this gives the bounding boxes
python inference_single_image.py --num_classes 2 --device cpu --model /home/user/sandbox/turtles/detr_base/turtle-detection/outputs/checkpoint.pth --conf_thresh 0.4 --image /home/user/sandbox/turtles/detr_base/turtle-detection/data/test/IMG_4320.JPG --json /home/user/sandbox/turtles/detr_base/turtle-detection/data/test/IMG_4320.json
