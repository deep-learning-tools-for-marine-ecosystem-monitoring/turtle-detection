# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import argparse
import datetime
import json
import random
import time
from pathlib import Path

import numpy as np
import torch
from torch.utils.data import DataLoader, DistributedSampler

import datasets
import util.misc as utils
from datasets import build_dataset, get_coco_api_from_dataset
from engine import evaluate, train_one_epoch
from models import build_model

from sklearn.cluster import DBSCAN

import torchvision.transforms as T
from skimage import io
from matplotlib import pyplot as plt
from PIL import Image

def get_args_parser():
    parser = argparse.ArgumentParser('Set transformer detector', add_help=False)
    parser.add_argument('--lr', default=1e-4, type=float)
    parser.add_argument('--lr_backbone', default=1e-5, type=float)
    parser.add_argument('--batch_size', default=2, type=int)
    parser.add_argument('--weight_decay', default=1e-4, type=float)
    parser.add_argument('--epochs', default=300, type=int)
    parser.add_argument('--lr_drop', default=200, type=int)
    parser.add_argument('--clip_max_norm', default=0.1, type=float,
                        help='gradient clipping max norm')
    
    # Model parameters
    parser.add_argument('--num_classes', type=int, default=None,
                        help="Number of classes in dataset+1")
    parser.add_argument('--frozen_weights', type=str, default=None,
                        help="Path to the pretrained model. If set, only the mask head will be trained")
    # * Backbone
    parser.add_argument('--backbone', default='resnet50', type=str,
                        help="Name of the convolutional backbone to use")
    parser.add_argument('--dilation', action='store_true',
                        help="If true, we replace stride with dilation in the last convolutional block (DC5)")
    parser.add_argument('--position_embedding', default='sine', type=str, choices=('sine', 'learned'),
                        help="Type of positional embedding to use on top of the image features")

    # * Transformer
    parser.add_argument('--enc_layers', default=6, type=int,
                        help="Number of encoding layers in the transformer")
    parser.add_argument('--dec_layers', default=6, type=int,
                        help="Number of decoding layers in the transformer")
    parser.add_argument('--dim_feedforward', default=2048, type=int,
                        help="Intermediate size of the feedforward layers in the transformer blocks")
    parser.add_argument('--hidden_dim', default=256, type=int,
                        help="Size of the embeddings (dimension of the transformer)")
    parser.add_argument('--dropout', default=0.1, type=float,
                        help="Dropout applied in the transformer")
    parser.add_argument('--nheads', default=8, type=int,
                        help="Number of attention heads inside the transformer's attentions")
    parser.add_argument('--num_queries', default=100, type=int,
                        help="Number of query slots")
    parser.add_argument('--pre_norm', action='store_true')

    # * Segmentation
    parser.add_argument('--masks', action='store_true',
                        help="Train segmentation head if the flag is provided")

    # Loss
    parser.add_argument('--no_aux_loss', dest='aux_loss', action='store_false',
                        help="Disables auxiliary decoding losses (loss at each layer)")
    # * Matcher
    parser.add_argument('--set_cost_class', default=1, type=float,
                        help="Class coefficient in the matching cost")
    parser.add_argument('--set_cost_bbox', default=5, type=float,
                        help="L1 box coefficient in the matching cost")
    parser.add_argument('--set_cost_giou', default=2, type=float,
                        help="giou box coefficient in the matching cost")
    # * Loss coefficients
    parser.add_argument('--mask_loss_coef', default=1, type=float)
    parser.add_argument('--dice_loss_coef', default=1, type=float)
    parser.add_argument('--bbox_loss_coef', default=5, type=float)
    parser.add_argument('--giou_loss_coef', default=2, type=float)
    parser.add_argument('--eos_coef', default=0.1, type=float,
                        help="Relative classification weight of the no-object class")

    # dataset parameters
    parser.add_argument('--dataset_file', default='coco')
    parser.add_argument('--coco_path', type=str)
    parser.add_argument('--coco_panoptic_path', type=str)
    parser.add_argument('--remove_difficult', action='store_true')

#     parser.add_argument('--output_dir', default='',
#                         help='path where to save, empty for no saving')
    parser.add_argument('--device', default='cuda',
                        help='device to use for inference')

    parser.add_argument('--image', type=str,
                        help='image to be processed')

    parser.add_argument('--json', type=str,
                        help='labelme annotation file if available')

    parser.add_argument('--model', type=str,
                        help='Path to the location of the model')

    parser.add_argument('--conf_thresh', default=0.85, type=float,
                        help='Path to the location of the model')

    return parser


# for output bounding box post-processing
def box_cxcywh_to_xyxy(x):
    x_c, y_c, w, h = x.unbind(1)
    b = [(x_c - 0.5 * w), (y_c - 0.5 * h),
         (x_c + 0.5 * w), (y_c + 0.5 * h)]
    return torch.stack(b, dim=1)

def rescale_bboxes(out_bbox, size):
    img_w, img_h = size
    b = box_cxcywh_to_xyxy(out_bbox)
    b = b * torch.tensor([img_w, img_h, img_w, img_h], dtype=torch.float32)
    return b


def detect(im, model, transform, conf_thresh):
    # mean-std normalize the input image (batch-size: 1)
    img = transform(im).unsqueeze(0)

    # demo model only support by default images with aspect ratio between 0.5 and 2
    # if you want to use images with an aspect ratio outside this range
    # rescale your image so that the maximum size is at most 1333 for best results
    print("image shape = ", img.shape)
    assert img.shape[-2] <= 1333 and img.shape[-1] <= 1333, 'could not support bigger images'

    # propagate through the model
    outputs = model(img)

    # keep only predictions with certain confidence
    probas = outputs['pred_logits'].softmax(-1)[0, :, :-1]
    prob_vals, prob_idx = probas.max(-1) #we have two classes BG(0), Turtle(2)

    #keep = probas.max(-1).values > 0.8
    keep = prob_vals > conf_thresh #probs thresh / conf thresh
    keep_idx = prob_idx==1
    keep = keep*keep_idx

    # convert boxes from [0; 1] to image scales
    bboxes_scaled = rescale_bboxes(outputs['pred_boxes'][0, keep], im.size)
    return probas[keep], bboxes_scaled


def save_json_annotations(gt_boxes, pred_boxes, ann_f, new_ann_dir):

    with open(ann_f) as f:
       json_obj = json.load(f)
       f.close()

    new_class = "Incorrect-prediction-Turtle"
    all_shapes = json_obj['shapes'] 


    gt = gt_boxes.detach().numpy()
    pred = pred_boxes.detach().numpy()

    matches_found_gt = np.zeros(gt.shape[0]).astype(int)

    for p in pred:
        match_found = False
        for idx, t in enumerate(gt):
            match_found = do_rects_intersect(p,t)
            if match_found:
                break
        if not match_found: #This is not a gt turtle - Add the shape to json
            s = {}
            s['label']      = new_class
            s['points']     = [ [int(p[0]), int(p[1])], [int(p[2]), int(p[3])] ]
            s['group_d']    = 0
            s["shape_type"] = "rectangle"
            s['flags']      = {}

            all_shapes.append(s)

    json_name = ann_f.split("/")[-1]
    new_ann_f = new_ann_dir+json_name

    with open(new_ann_f, 'w') as f:
       json.dump(json_obj, f)
       f.close()


def load_json_annotations(json_f):
    with open(json_f) as f:
        json_obj = json.load(f)
        f.close()
   
    label_bboxes = []
    for s in json_obj['shapes']:
        if s['label'] == "Turtle":
            #print(s['points'])
            bbox = s['points'][0] + s['points'][1]
            label_bboxes += [bbox]

    return torch.FloatTensor(label_bboxes) 


#Utilized  from https://www.geeksforgeeks.org/find-two-rectangles-overlap/
def do_rects_intersect(r1, r2):

    l1_x, l1_y, r1_x, r1_y = r1
    l2_x, l2_y, r2_x, r2_y = r2
       
    if (l1_x == r1_x or l1_y == r1_y or l2_x == r2_x or l2_y == r2_y):
        # the line cannot have positive overlap
        return False
       
     
    # If one rectangle is on left side of other
    if(l1_x >= r2_x or l2_x >= r1_x):
        return False
 
    # If one rectangle is above other (In image coordinate, y increases from top to bottom)
    if(r1_y <= l2_y or r2_y <= l1_y):
        return False
 
    return True



def get_confusion_matrix(gt_boxes, pred_boxes):
    gt = gt_boxes.detach().numpy()
    pred = pred_boxes.detach().numpy()

    matches_found_gt = np.zeros(gt.shape[0]).astype(int)

    tp = 0 #Correct prodection of turtle
    fn = 0 #Is A turtle, but not predicted
    fp = 0 #Is Not a turtle, but detected as Turtle

    for p in pred:
        match_found = False
        for idx, t in enumerate(gt):
            match_found = do_rects_intersect(p,t)
            if match_found:
                #print("MATCH FOUND = ", match_found)
                matches_found_gt[idx] += 1
                tp+=1
                break
        if not match_found:
            fp += 1

    fn = np.count_nonzero(matches_found_gt == 0)
    print("\n*********************************************\n")
    print("Trutles Correctly Found = \t\t", tp)
    print("Trutles Not Found = \t\t\t", fn) 
    print("Incorrectly detected as a Turtle = \t", fp)
    print("\n*********************************************\n")



def remove_repeated_predictions(boxes, prob, dist_thresh=1):
    #print(boxes.shape, prob.shape)
    X = boxes.detach().numpy()
    np_prob = prob.detach().numpy()
    db = DBSCAN(eps=dist_thresh, min_samples=1).fit(X)
    labels = db.labels_

    unique_labels = (np.unique(labels))
    new_bboxes = []
    new_probs = []
    for ul in unique_labels:
        indexes = np.where(labels==ul)

        points = X[indexes]
        mean_bbox = np.mean(points, axis=0)
        new_bboxes += [mean_bbox.tolist()]

        label_probs = np_prob[indexes]
        mean_prob = np.mean(label_probs, axis=0)
        new_probs += [mean_prob.tolist()]

    return torch.FloatTensor(new_bboxes), torch.FloatTensor(new_probs) 


from matplotlib.patches import Patch

def plot_results_and_gt(pil_img, prob, boxes, gt_boxes):
    CLASSES = ['N/A', 'Turtle']
    GT_CLASSES = ['N/A', 'Ground-Truth']
    # colors for visualization
    COLORS = [[1., 0., 0.]]
    GT_COLORS = [[0., 1., 0.]]
    #plt.figure(figsize=(16,10))
    #plt.figure()
    ax = plt.gca()
    for p, (xmin, ymin, xmax, ymax), c in zip(prob, boxes.tolist(), COLORS * 100):
        ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                   fill=False, color=c, linewidth=1))
        cl = p.argmax()

    for (xmin, ymin, xmax, ymax), c in zip(gt_boxes.tolist(), GT_COLORS * 100):
        ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                   fill=False, color=c, linewidth=1))


    legend_elements = [ Patch(facecolor='white', edgecolor='g',
                                         label='Ground-truth'),
                        Patch(facecolor='white', edgecolor='r',
                                     label='Prediction')]

    ax.legend(handles=legend_elements, loc='upper right', bbox_to_anchor=(1.0, 1.1))

    plt.imshow(pil_img)
    plt.axis('off')
    plt.show()


def plot_results(pil_img, prob, boxes):
    CLASSES = ['N/A', 'Turtle']
    # colors for visualization
    COLORS = [[0.850, 0.325, 0.098]]
    plt.imshow(pil_img)
    ax = plt.gca()
    for p, (xmin, ymin, xmax, ymax), c in zip(prob, boxes.tolist(), COLORS * 100):
        ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                   fill=False, color=c, linewidth=1))
        cl = p.argmax()
        text = f'{CLASSES[cl]}: {p[cl]:0.2f}'

    plt.axis('off')
    plt.show()
    

def main(args):
    utils.init_distributed_mode(args)
    print(args)

    device = torch.device(args.device)

    model, criterion, postprocessors = build_model(args)
    #print(model)
    checkpoint_path = args.model

    print(checkpoint_path)
    checkpoint = torch.load(checkpoint_path, map_location=device)
    model.load_state_dict(checkpoint['model'])
    model.eval()
    model.to(device)


    # Do inference steps
    file_name = args.image
    I = Image.open(file_name)
    #plt.axis('off')
    #plt.imshow(I)
    #plt.show()

    # standard PyTorch mean-std input image normalization
    transform = T.Compose([
        T.Resize(1000, max_size=1333),
        T.ToTensor(),
        T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    labelme_ann_f = args.json
    #print("annotation_f: ", labelme_ann_f)
    if labelme_ann_f:
        gt_bboxes = load_json_annotations(labelme_ann_f)
        print("Ground truth bboxes:")
        print(gt_bboxes)

        scores, boxes = detect(I, model, transform, args.conf_thresh)

        num_predictions = scores.size()[0]
        if num_predictions > 0:
            new_boxes, new_scores = remove_repeated_predictions(boxes, scores)
        else:
            new_boxes  = boxes
            new_scores = scores

        print("Predicted bboxes:")
        print(new_boxes)
        get_confusion_matrix(gt_bboxes, new_boxes)
        
        #new_ann_dir = "/home/user/sandbox/turtles/detr_base/detr_pre-_rebecca_cleaning/results/new_annotations/"
        #save_json_annotations(gt_bboxes, new_boxes, labelme_ann_f, new_ann_dir)

        plot_results_and_gt(I, new_scores, new_boxes, gt_bboxes)


    else:
        scores, boxes = detect(I, model, transform, conf_thresh)
        num_predictions = scores.size()[0]
        if num_predictions > 0:
            new_boxes, new_scores = remove_repeated_predictions(boxes, scores)
        else:
            new_boxes  = boxes
            new_scores = scores
        plot_results(I, new_scores, new_boxes)



if __name__ == '__main__':
    parser = argparse.ArgumentParser('DETR inference script', parents=[get_args_parser()])
    args = parser.parse_args()
#     if args.output_dir:
#         Path(args.output_dir).mkdir(parents=True, exist_ok=True)
    main(args)
